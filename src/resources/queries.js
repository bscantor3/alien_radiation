const Queries = {
  saveValidation:
    `INSERT INTO validations(structure, anomaly) 
    VALUES ($1,$2) RETURNING created;`,

  getStats:
    `SELECT 
    COUNT(anomaly) filter (where anomaly is true) as count_anomalies,
    COUNT(anomaly) filter (where anomaly is false) as count_no_anomalies
  FROM public.validations;`
}

module.exports = Queries;
