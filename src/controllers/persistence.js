const { Pool } = require("pg");

const Queries = require("../resources/queries");

/**
 * Persitence Controller
 */
class Persistence {

  /**
   * Inicialize with postgress pool and error contention event 
   */
  constructor() {
    this.pool = new Pool();

    this.pool.on("error", (err) => {
      console.log("Unexpected error on Client", err);
      process.exit(-1);
    })
  }

  /**
   * Saving validation info: structure and anomaly
   * 
   * @param {Array} structure   Matrix ARN
   * @param {Boolean} anomaly   Identify if there is an anomaly
   * 
   * @return {Object}  Result saving record
   */
  saveValidation = async (structure, anomaly) => {
    const { rows } = await this.pool.query(Queries.saveValidation, [structure, anomaly]);
    return rows[0];
  }

  /**
   * Get Stats by number of anomalies
   * 
   * @return {Object}  Result stats anomalies
   */
  getStats = async () => {
    let { rows } = await this.pool.query(Queries.getStats);

    rows[0].count_anomalies = Number(rows[0].count_anomalies);
    rows[0].count_no_anomalies = Number(rows[0].count_no_anomalies);

    rows[0].ratio = rows[0].count_anomalies == 0 ? 0 : (rows[0].count_anomalies / (rows[0].count_anomalies + rows[0].count_no_anomalies));

    return rows;
  }

}

module.exports = Persistence;