const { Router } = require('express');
const router = Router();

const Persistence = require('../controllers/persistence');
const { isAnomaly } = require("../logic/anomaly");

const data = new Persistence();

//POST: /validate-anomaly
router.post('/validate-anomaly', async (req, res, next) => {
  try {
    const { dna } = req.body;

    console.log(dna);

    if (dna.length < 2 || dna.length > 2000) {
      return res.status(400);
    }

    let anomaly = isAnomaly(dna)

    await data.saveValidation(dna, anomaly);
    if (anomaly) {
      return res.status(200).send();
    } else {
      return res.status(403).send();
    }

    //CODE
  } catch (e) {
    console.log(e);
    next(e);
  }
});


//GET: /stats
router.get('/stats', async (req, res) => {
  let row = await data.getStats();

  console.log(row);

  let response = {
    count_anomalies: row[0].count_anomalies,
    count_no_anomalies: row[0].count_no_anomalies,
    ratio: row[0].ratio
  }

  return res.json(response);

});

module.exports = router;