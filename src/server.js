const chalk = require('chalk');

const app = require('./app');

const port = process.env.PORT || 2020;

app.listen(port, () => {
  console.info(
    `🚀 🚀 🚀 ALIEN_RADIATION ${chalk.green(
      process.env.NODE_ENV.toUpperCase()
    )} in ${chalk.blue(`http://localhost:${port}`)}`
  );
})