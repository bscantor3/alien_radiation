
//Function detection anomaly
const isAnomaly = (array) => {
  try {

    const SIZE = array.length;

    //Row DNA
    for (let i = 0; i < SIZE; i++) {

      //Value
      for (let j = 0; j < SIZE; j++) {

        if (j + 2 < SIZE && array[i][j] == array[i][j + 1] && array[i][j] == array[i][j + 2]) {
          console.log("Horizontal");
          return true;
        }

        if (i + 2 < SIZE && array[i][j] == array[i + 1][j] && array[i][j] == array[i + 2][j]) {
          console.log("Vertical");
          return true;
        }

        if (i + 2 < SIZE && j + 2 < SIZE && array[i][j] == array[i + 1][j + 1] && array[i][j] == array[i + 2][j + 2]) {
          console.log("Diagonal derecha");
          return true;
        }

        if (i - 2 >= 0 && j - 2 >= 0 && array[i][j] == array[i - 1][j - 1] && array[i][j] == array[i - 2][j - 2]) {
          console.log("Diagonal izquierda");
          return true;
        }
      }
    }

    return false;

  } catch (e) {
    return false;
  }
}

module.exports = { isAnomaly };