CREATE DATABASE "alien-radiation";

\c "alien-radiation";

CREATE TABLE public.validations (
  id serial PRIMARY KEY,
  structure text [] NOT NULL,
  anomaly boolean NOT NULL DEFAULT false,
  created timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);