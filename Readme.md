# Alien-radiation

La solución propuesta fue desarrollada con Node.js LTS, Express y PostgreSQL . La distribución de carpetas tiene presente la clasificación
de utilidades dentro del sistema en lo que incluye, routes, logics, controllers y resources.

### Instrucciones de uso:

Como requisito se debe tener Node.js LTS, yarn y Docker Compose. La base de datos se encuentra pre-configurada en la carpeta config, se puede encontrar como **alien-radiation**.

Las configuraciónes de entorno anexadas como parte de la prueba, están listas para modo **desarrollo** (.env.development), **test** (.env.test) y **producción** (.env).

La url del enpoint es */api/v1* y sus rutas son */validate-anomaly* y */stats*.

###### Para inicializar la base de datos PostgreSQL
```
docker-compose -f docker-compose.yml up -d postgresql
```

###### Prueba en desarrollo local: http://localhost:2020/api/v1
```
yarn install
yarn start:dev
```

###### Despliegue local con Docker: http://localhost:3000/api/v1
```
docker-compose -f docker-compose.yml up -d
```

###### Consideraciones finales

Para el proyecto se pensó en una propuesta que implementara pruebas unitarias con JEST, pero por falta de tiempo
personal queda como un pendiente para revisar y estudiar.

A su vez, un plus dentro del desarrollo del proyecto fue la implementación de Docker con servicios de POSTGRESQL, 
NODE JS y PGADMIN (Para visualización de la data con IDE, queda configurado para su posterior uso si así se requiere) 

###### Imágenes del Desarrollo propuesto

<p align="center">
  <img src="images/docker.png" />
</p>

<p align="center">
  <img src="images/postman_api_1.png" />
</p>

<p align="center">
  <img src="images/postman_api_2.png" />
</p>

<p align="center">
  <img src="images/postman_api_3.png" />
</p>